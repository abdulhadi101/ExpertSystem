/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expertsystem;


public class SecondSection extends javax.swing.JFrame {
    
    public static String L = "What classes of food do you take during Lauch ?" ;
    public static String D = "What classes of food do you take during Dinner ?" ;
    public static SecondSection stnds = new  SecondSection(); 
        
    public SecondSection() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jCheckBoxCb = new javax.swing.JCheckBox();
        jCheckBoxPr = new javax.swing.JCheckBox();
        jButtonNext1 = new javax.swing.JButton();
        jLabelDisplay = new javax.swing.JLabel();
        jCheckBoxWtr = new javax.swing.JCheckBox();
        jCheckBoxFts = new javax.swing.JCheckBox();
        jButtonNext2 = new javax.swing.JButton();
        jButtonNext3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jCheckBoxCb.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jCheckBoxCb.setText("Carbohydrate");

        jCheckBoxPr.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jCheckBoxPr.setText("Protein");

        jButtonNext1.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jButtonNext1.setText("Next");
        jButtonNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNext1ActionPerformed(evt);
            }
        });

        jLabelDisplay.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabelDisplay.setText("What classes of food do you take for Breakfast");

        jCheckBoxWtr.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jCheckBoxWtr.setText("Water");

        jCheckBoxFts.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jCheckBoxFts.setText("Fat and Oil");

        jButtonNext2.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jButtonNext2.setText("Next");
        jButtonNext2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNext2ActionPerformed(evt);
            }
        });

        jButtonNext3.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jButtonNext3.setText("Next");
        jButtonNext3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNext3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelDisplay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButtonNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonNext2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonNext3, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jCheckBoxPr)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxCb)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxFts)))
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBoxWtr)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxPr)
                    .addComponent(jCheckBoxCb)
                    .addComponent(jCheckBoxFts)
                    .addComponent(jCheckBoxWtr))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonNext1)
                    .addComponent(jButtonNext2)
                    .addComponent(jButtonNext3))
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNext1ActionPerformed
        deSet12();
        jLabelDisplay.setText("");
        jLabelDisplay.setText(L);             
    }//GEN-LAST:event_jButtonNext1ActionPerformed

    private void jButtonNext2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNext2ActionPerformed
        deSet23();
        jLabelDisplay.setText("");
        jLabelDisplay.setText(D);        
    }//GEN-LAST:event_jButtonNext2ActionPerformed

    private void jButtonNext3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNext3ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonNext3ActionPerformed

    public static void main(String args[]) {
        
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SecondSection().setVisible(true);
            }
        });
    }
    
    public void setCheckEmpty(){
    
        jCheckBoxPr.setAction(null);
        jCheckBoxCb.setAction(null);
        jCheckBoxCb.setAction(null);
        jCheckBoxFts.setAction(null);
        jCheckBoxWtr.setAction(null);
        
    }
    
    public void deSet23(){
    
        jButtonNext2.setVisible(false);
        jButtonNext3.setVisible(true);
        
    }
    
    public void deactivate23(){
    
        jButtonNext2.setVisible(false);
        jButtonNext3.setVisible(false);
        
    }
    
    public void deSet12(){
    
        jButtonNext1.setVisible(false);
        jButtonNext2.setVisible(true);
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonNext1;
    private javax.swing.JButton jButtonNext2;
    private javax.swing.JButton jButtonNext3;
    private javax.swing.JCheckBox jCheckBoxCb;
    private javax.swing.JCheckBox jCheckBoxFts;
    private javax.swing.JCheckBox jCheckBoxPr;
    private javax.swing.JCheckBox jCheckBoxWtr;
    private javax.swing.JLabel jLabelDisplay;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
